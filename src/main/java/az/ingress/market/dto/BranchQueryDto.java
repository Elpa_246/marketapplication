package az.ingress.market.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BranchQueryDto {
    private Long branchId;
    private String branchName;
    private String addressName;
    private String marketName;
}
