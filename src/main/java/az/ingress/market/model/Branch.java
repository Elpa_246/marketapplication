package az.ingress.market.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    String branchName;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "m_id",referencedColumnName = "id")
    Market market;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "a_id",referencedColumnName = "id")
    Address address;

}
