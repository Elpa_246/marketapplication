package az.ingress.market.repository;

import az.ingress.market.dto.BranchQueryDto;
import az.ingress.market.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BranchRepository extends JpaRepository<Branch,Long> {
    @Query(value = "select new az.ingress.market.dto.BranchQueryDto(b.id, b.branchName, a.addressName, m.marketName) from Branch b join b.market m join b.address a")
    List <BranchQueryDto> getBranchWithCustomDtoQuery();

//    @Query(value = "select b from Branch b join fetch b.market m join fetch b.address a")
//    Branch getBranchJpql();

}
