package az.ingress.market.controller;

import az.ingress.market.dto.BranchQueryDto;
import az.ingress.market.dto.BranchRequestDto;
import az.ingress.market.model.Branch;
import az.ingress.market.service.BranchService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchController {

    private final BranchService branchService;
//    @GetMapping("/{id}")
//    private String getBranchById(@PathVariable Long id){
//        return branchService.getBranchByid(id);
//    }
    @GetMapping
    public List<BranchQueryDto> getBranch() {
        return branchService.getBranch();
    }

    @PostMapping
    public Long createBranch(@RequestBody BranchRequestDto request){
        return branchService.createBranch(request);
    }
}
