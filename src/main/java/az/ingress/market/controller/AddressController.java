package az.ingress.market.controller;

import az.ingress.market.dto.AddressRequestDto;
import az.ingress.market.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;

    @PostMapping
    public Long createBranch(@RequestBody AddressRequestDto request){
        return addressService.createAddress(request);
    }
}
