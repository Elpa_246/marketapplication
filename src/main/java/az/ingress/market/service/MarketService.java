package az.ingress.market.service;

import az.ingress.market.dto.MarketRequestDto;
import az.ingress.market.dto.MarketResponseDto;
import az.ingress.market.model.Market;

import java.util.List;

public interface MarketService {
    Long createMarket(MarketRequestDto request);

    List<MarketResponseDto> getMarket();

    Market getMarketWithId(Long id);
}
