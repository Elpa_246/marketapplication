package az.ingress.market.service;

import az.ingress.market.dto.AddressRequestDto;
import az.ingress.market.model.Address;

public interface AddressService {
    Long createAddress(AddressRequestDto request);

    Address findById(long id);
}
