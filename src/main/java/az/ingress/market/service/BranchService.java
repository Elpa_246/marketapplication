package az.ingress.market.service;

import az.ingress.market.dto.BranchQueryDto;
import az.ingress.market.dto.BranchRequestDto;
import az.ingress.market.model.Branch;

import java.util.List;

public interface BranchService {
    Long createBranch(BranchRequestDto request);

//    String getBranchByid(Long id);

    List<BranchQueryDto> getBranch();
}
