package az.ingress.market.service.impl;

import az.ingress.market.config.AppConfiguration;
import az.ingress.market.dto.AddressRequestDto;
import az.ingress.market.model.Address;
import az.ingress.market.repository.AddressRepository;
import az.ingress.market.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {
    private final AddressRepository addressRepository;
    private final ModelMapper modelMapper;

    @Override
    public Address findById(long id) {
        Optional<Address> addressOptional = addressRepository.findById(id);
        addressOptional.orElseThrow(() -> new RuntimeException());
        return addressOptional.get();
    }


    @Override
    public Long createAddress(AddressRequestDto request) {
        Address address = modelMapper.map(request, Address.class);
        return addressRepository.save(address).getId();
    }

}
