package az.ingress.market.service.impl;

import az.ingress.market.config.AppConfiguration;
import az.ingress.market.dto.BranchQueryDto;
import az.ingress.market.dto.BranchRequestDto;
import az.ingress.market.model.Address;
import az.ingress.market.model.Branch;
import az.ingress.market.model.Market;
import az.ingress.market.repository.BranchRepository;
import az.ingress.market.service.AddressService;
import az.ingress.market.service.BranchService;
import az.ingress.market.service.MarketService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class BranchServiceImpl implements BranchService {
    private final BranchRepository branchRepository;
    private final ModelMapper modelMapper;
    private final AddressService addressService;
    private final MarketService marketService;
    private final AppConfiguration appConfiguration;

//    @Override
//    public String getBranchByid(Long id) {
//        Branch branch = branchRepository.findById(id).get();
//        return branch.getBranchName();
//    }

    @Override
    public List<BranchQueryDto> getBranch() {
        List<BranchQueryDto> branchQueryDto = branchRepository.getBranchWithCustomDtoQuery();
        log.info("Get branch from DB : {}", branchQueryDto);
        return branchQueryDto;
    }

    @Override
    public Long createBranch(BranchRequestDto request) {
//        Address address =addressService.findById(request.getAddress_id());
//        Market market = marketService.getMarketWithId(request.getMarket_id());
//        Branch branch = Branch.builder()
//                .branchName(request.getBranchName())
//                .address(address)
//                .market(market)
//                .build();
        Branch map = modelMapper.map(request, Branch.class);
        return branchRepository.save(map).getId();
    }


}
